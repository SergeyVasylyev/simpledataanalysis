Manual Postgres DB start with docker (you can use manual way or docker-compose):

1. get postgres

    docker pull postgres
    
    - or with tag, for smaller size:
    
    docker pull postgres:alpine
2. run postgres

    docker run --name postgres -p 5432:5432 -d -e POSTGRES_PASSWORD=root postgres
    
3. inside the container

    docker exec -it postgres bash
    
    psql -U postgres
    
    \du
    
    \l
4. discover postgres port

    docker inspect postgres -f "{{json .NetworkSettings.Networks }}"
5. pgAdmin (UI for postgres DB)

    docker pull dpage/pgadmin4

    docker run -p 5555:80 --name pgadmin -e PGADMIN_DEFAULT_EMAIL="user@sda.com" -e PGADMIN_DEFAULT_PASSWORD="rootroot" dpage/pgadmin4
6. access to pgAdmin through the browser: http://localhost:5555/ 